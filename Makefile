JFLAGS = -g -d class/ -cp src
JAVAC=javac
FLAGS = cvf
JAR=jar

main: src/CarSimulator.java src/Car.java src/Highway.java
	$(JAVAC) $(JFLAGS) src/*.java
	$(JAR) $(FLAGS) Simulator.jar class/*.class

clean :
	rm -rf class/*

run: main
	appletviewer class/index.html
