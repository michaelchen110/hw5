import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;
import java.util.Calendar;
import java.util.ArrayList;


public class CarSimulator extends Applet implements ActionListener
{
    private Image car_IMG, highway_IMG, fire_IMG;
    private int task, length = 1000, carNumber, iCarNumber;
    private Highway highway;
    private boolean safe = true;
    private int SPEED;
    
    private Button btn;
    private Choice ch, ch1, ch2;

    public void init() // init() method
    {
        btn=new Button("Start");
        btn.addActionListener(this);
        add(btn);

        ch2 = new Choice();
        ch2.addItem("Normal");
        ch2.addItem("Slow");
        ch2.addItem("Fast");
        this.add(ch2);


        ch = new Choice();
        ch.addItem("1 Car");
        ch.addItem("5 Cars");
        ch.addItem("10 Cars");
        ch.addItem("15 Cars");
        ch.addItem("20 Cars");
        this.add(ch);

        ch1 = new Choice();
        ch1.addItem("0 Intersec-cars");
        ch1.addItem("2 Intersec-cars");
        ch1.addItem("4 Intersec-cars");
        ch1.addItem("6 Intersec-cars");
        ch1.addItem("8 Intersec-cars");
        ch1.addItem("10 Intersec-cars");
        this.add(ch1);

        car_IMG = getImage(getCodeBase(),"car.jpg");
        highway_IMG = getImage(getCodeBase(),"highway.jpg");
        fire_IMG = getImage(getCodeBase(),"fire.jpg");
    }
    public void paint(Graphics g)  
    {
        try{
            if (btn.getLabel() == "Stop") {
                ArrayList<Integer> now = highway.nowHighway();
                if (now == null || !safe) {
                    safe = false;
                    g.drawImage(fire_IMG, 400, 50, this);
                }
                else {
                    for (int i = 0; i < length; i++)
                        g.drawImage(highway_IMG, 20 * i, 50, this);
                    for (int i = 0; i < now.size(); i++) 
                        g.drawImage(car_IMG, 30 * now.get(i), 65, this);
                }
            }
        }
        catch (Exception a){
            System.out.println(btn.getLabel() + " Jizz in my paints");
        }
    }
    public void actionPerformed(ActionEvent e)
    {
        if (btn.getLabel() == "Start") {
            btn.setLabel("Stop");
            carNumber =5 * ch.getSelectedIndex();
            iCarNumber =2 * ch1.getSelectedIndex();
            if (ch2.getSelectedIndex() == 0) 
                SPEED = 600;
            else if (ch2.getSelectedIndex() == 1) 
                SPEED = 900;
            else
                SPEED = 300;

            if (carNumber == 0) carNumber = 1;
            highway = new Highway(SPEED, carNumber, iCarNumber);

            Thread adder = new Thread(new Runnable() {
                        public void run() 
                        {
                            while (btn.getLabel() == "Stop" && safe) {
                                try {
                                    highway.insert(0);
                                    highway.insert(20);
                                    Thread.sleep(SPEED);
                                }
                                catch (InterruptedException e) {}
                            }
                         }
            });
            Thread painter = new Thread(new Runnable() {
                    public void run()
                    {
                        while (btn.getLabel() == "Stop" && safe) {
                            try {
                                repaint();
                                Thread.sleep(50);
                            }
                            catch (InterruptedException e) {}
                        }
                    }
            });
            painter.start();
            adder.start();
        }
        else if (btn.getLabel() == "Stop") {
            btn.setLabel("");
        }
        else {
            Toolkit.getDefaultToolkit().beep();
        }
    }
}
