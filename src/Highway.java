import java.util.ArrayList;
public class Highway
{
    private int length;
    private Car head, tail;
    private int carNum, iCarNum;
    private int SPEED;
    public Highway(int s, int car, int iCar)
    {
        length = 1000;
        carNum = car;
        iCarNum = iCar;
        head = null;
        tail = null;
        SPEED = s;
    }
    public ArrayList<Integer> nowHighway()
    {   
        ArrayList<Integer> visibleCars = new ArrayList<Integer>();
        Car tmp = tail;
        while (tmp != head) {
            if (tmp.getPosition() > tmp.next.getPosition()) 
                return null;
            visibleCars.add(tmp.getPosition());
            tmp = tmp.next;
        }
        visibleCars.add(head.getPosition());
        return visibleCars;
    }
        
    public void insert(int pos)
    {
        if (carNum == 0 && pos == 0) 
            return;
        else if (iCarNum == 0 && pos == 20) 
            return;

        Car newCar = new Car(pos, SPEED);
        if (pos == 0)
            carNum--;
        else
            iCarNum--;

        if (head == null) {
            head = newCar;
            tail = head;
        }
        else { //Insertion sort
            if (pos < tail.getPosition()) {
                newCar.next = tail;
                tail = newCar;
            }
            else if (pos > head.getPosition()) {
                head.next = newCar;
                head = head.next;
            }
            else {
                Car tmp = tail;
                while (tmp != head) {
                    if (pos > tmp.getPosition() && pos < tmp.next.getPosition()) {
                        newCar.next = tmp.next;
                        tmp.next = newCar;
                        break;
                    }
                    tmp = tmp.next;
                }
            }
        }
        newCar.start();
    }
}
