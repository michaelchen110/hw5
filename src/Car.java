public class Car extends Thread
{
    public Car next;
    private int decision;
    private int speed;
    private int newSpeed;
    private int position;
    private int SPEED;

    public Car(int pos, int s)
    {
        next = null;
        decision = 0;
        speed = 1;
        newSpeed = 0;
        position = pos;
        setSpeed(s);
    }
    public void move()
    {
        position++;
    }
    public void decide()
    {   
        int distance = (next == null) ? 8 : next.getPosition() - position;
        int vector = (next == null) ? 4 : speed - next.getSpeed();
        if (decision == 0 ) {
            if (newSpeed > 0) {
                speed = newSpeed;
                newSpeed = 0;
            }
            if (2 * speed > distance) { //down
                decision = 1;
                if (distance < 2)
                    newSpeed = 1;
                else 
                    newSpeed = distance / 2;
            }
            else if (2 * speed < distance) { //up
                decision = 2;
                if (distance >= 8) 
                    newSpeed = 4;
                else
                    newSpeed = distance / 2;
            }
        }
        else {
            decision--;
        }
    }
    public void setSpeed(int s)
    { 
        SPEED = s;
    }
    public int getSpeed()
    {
        return speed;    
    }
    public int getPosition()
    {
        return position;
    }
    public void run()
    {
        while (position < 300) {
            decide();
            for (int i = 0; i < speed; i++) {
                try {
                    move();
                    Thread.sleep(SPEED / speed);
                }
                catch (Exception e) {}
            }
        }
    }
}
